# Shmup One

## Introduction

**Shmup One** is a reproduction of the little Shoot'em Up game presented in the
tutorial ["Creating a 2D Game with Unity"](https://pixelnest.io/tutorials/2d-game-unity/)
made by [Pixelnest Studio](https://pixelnest.io) in 2013. This tutorial was firstly posted on
the [subforum for developers](https://forum.canardpc.com/threads/84568-Tuto-faire-un-jeu-2D-avec-Unity)
of the PC gaming magazine Canard PC, and updated until 2016.
The redaction of Canard PC will later use this tutorial in the magazine itself in a serie of tutorials
on the 2D engine of Unity using their own assets.

### Why doing this tutorial

Thanks to the student club [Centrifuge - Game Lab](https://amicale-insat.fr/clubs/150/view) of the [INSA of Toulouse](http://www.insa-toulouse.fr/en/index.html), I had the occasion
to participate in an introduction on Unity and particularly on its 3D Engine, by making a small Breakout game.
I then wanted to realize a reproduction of the Google's **T-Rex Run!** game available on the Chrome browser, this
choice being motivated by the fact that [I have already made it in Python](https://gitlab.com/Rhaegar_Stormbringer/googleGame) - without graphics. As this game is
a 2D game, it seemed appropriate to use the 2D rather than the 3D engine of Unity to made it, and so I take this tutorial in order
to quickly get used to it.

### Assets

Pixelnest Studio provides all the sprites and sounds used by the game. They have been used for the game ["The Great Paper Adventure"](https://team-monique.itch.io/the-great-paper-adventure) made by [Team Monique](https://team-monique.itch.io/) and described as :
> A funny papered 2D shoot-them-up made with XNA, released on PC during December 2010 and on Xbox during June 2011

![some examples of sprites provided by Pixelnest Studio](Doc/sprites_examples.png "Some examples of sprites provided by Pixelnest Studio")

The source code, in C#, is displayed bit by bit in the course of the tutorial but not provided - same for the prefabs, scenes or UI elements. The tutorial was supposed to be made using the Unity 4.x Personal Edition, but was updated in 2016 to be compatible with the Unity 5.x. I personally used the latest LTS release, Unity 2018.4.

### Licensing

**Shmup One** is a reproduction of the game presented in the tutorial, minor some changes in the scripts due to a most recent version of Unity that the one
used by Pixelnest Studio in 2013. As stated at the begining of the tutorial :
> The assets and arts are licensed under a CC-BY-NC attribution. They have been made by Thibault Person for the game The Great Paper Adventure.
The source code belongs to Pixelnest Studio and is released under a MIT license.
Get more information about our licenses on GitHub.
The source code is available on our GitHub repository.
