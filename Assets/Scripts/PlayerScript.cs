﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
	public Vector2 speed = new Vector2(50, 50);
	private Vector2 movement;
	private Rigidbody2D rigidbodyComponent;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		bool shoot = Input.GetButtonDown("Fire1");
		
		if (shoot)
		{
			WeaponScript weapon = GetComponent<WeaponScript>();
			if (weapon != null)
			{
				// false to avoid autodestruction of the player
				weapon.Attack(false);
				SoundEffectsHelper.Instance.MakePlayerShotSound();
			}
		}
		
		// Retrieve input by axis
		float inputX = Input.GetAxis("Horizontal");
		float inputY = Input.GetAxis("Vertical");
		
		// speed modification
		movement = new Vector2(speed.x * inputX, speed.y * inputY);
		
		// --- Camera bound limitation ---
		var dist = (transform.position - Camera.main.transform.position).z;
		var leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		var rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		var topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).y;
		var bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, dist)).y;

		transform.position = new Vector3(Mathf.Clamp(transform.position.x, leftBorder, rightBorder), Mathf.Clamp(transform.position.y, topBorder, bottomBorder), transform.position.z);
    }
	
	void FixedUpdate()
	{
		// retrieve component and store the reference
		if (rigidbodyComponent == null)
		{
			rigidbodyComponent = GetComponent<Rigidbody2D>();
		}
		
		// move the game object
		rigidbodyComponent.velocity = movement;
	}
	
	void OnCollisionEnter2D(Collision2D collision)
	{
		bool damagePlayer = false;

		// Collision with enemy
		EnemyScript enemy = collision.gameObject.GetComponent<EnemyScript>();
		if (enemy != null)
		{
			// Kill the enemy
			HealthScript enemyHealth = enemy.GetComponent<HealthScript>();
			if (enemyHealth != null)
			{
				enemyHealth.Damage(enemyHealth.hp);
			}
	
			damagePlayer = true;
		}

		// Damage the player
		if (damagePlayer)
		{
			HealthScript playerHealth = this.GetComponent<HealthScript>();
			if (playerHealth != null)
			{
				playerHealth.Damage(1);
			}
		}
	}
	
	void OnDestroy()
	{
    // Game Over UI
    var gameOver = FindObjectOfType<GameOverScript>();
    gameOver.ShowButtons();
	}
}
