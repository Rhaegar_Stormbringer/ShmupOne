﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour
{
	public int hp = 1;
	public bool isEnemy = true;

	public void Damage(int dmg)
	{
		hp -= dmg;
		if (hp <= 0)
		{
			SpecialEffectsHelper.Instance.Explosion(transform.position);
			SoundEffectsHelper.Instance.MakeExplosionSound();
			Destroy(gameObject);
		}
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		// check if shot type
		BulletScript shot = other.gameObject.GetComponent<BulletScript>();
		if (shot != null)
		{
			// avoid friendly fire
			if (shot.isEnemyShot != isEnemy)
			{
				Damage(shot.damage);
				
				// destroy projectile
				Destroy(shot.gameObject);
			}
		}
	}
}
